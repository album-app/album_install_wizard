# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['album_install_wizard.py'],
			 pathex=[],
			 binaries=[],
			 datas=[('album.yml', '.'),('Miniconda3-v4_12-Linux-x86_64.sh','.')],
			 hiddenimports=[],
			 hookspath=[],
			 hooksconfig={},
			 runtime_hooks=[],
			 excludes=[],
			 win_no_prefer_redirects=False,
			 win_private_assemblies=False,
			 cipher=block_cipher,
			 noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
			 cipher=block_cipher)

exe = EXE(pyz,
		  a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
		  name='album_install_wizard',
		  debug=False,
		  bootloader_ignore_signals=False,
		  strip=False,
		  upx=True,
		  console=True,
		  disable_windowed_traceback=False,
		  target_arch=None,
		  codesign_identity=None,
		  entitlements_file=None )
