# CI Routine for the album framework.


#----------------------------
# templates
#----------------------------

# Windows base template
#
# Downloads conda package in <current_working_directory>\downloads if package not present.
# Installs conda in >current_working_directory>\miniconda if conda not already installed.
# Runs conda initialization and configuration.
#
# NOTE: Apparently there is no output for commands split over several lines...
.windows_base_template:
  before_script:
    - 'echo "We are in path: $pwd "'
    - 'echo "conda URL:  $env:MINICONDA_URL "'
    - '$oldProgressPreference = $progressPreference; $progressPreference = "SilentlyContinue";'
    - 'if(-Not (Test-Path .\download)) {echo "Cache download not found! Creating..."; New-Item -ItemType Directory -Force -Path .\download} else { echo ".\download cache found! with content:"; Get-ChildItem -Path .\download}'                                                                                                                                                # create cache dir
    - '[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12'                                                                                                                                                                                                                                                                                          # set Security download protocol to avoid https errors
    - 'if(Test-Path .\download\miniconda_url.txt) {echo "found previous miniconda url information. Extracting information..."; $miniconda_cache_url = try {Get-Content -Path .\download\miniconda_url.txt -errorAction Stop} catch {echo "None"}; echo "Cached minconda package: $miniconda_cache_url"} else { echo ".\download\miniconda_url.txt does not exist!"}'          # read url from cache
    - 'if($miniconda_cache_url -ne $env:MINICONDA_URL) {echo "Cached URL not equal to given URL. Removing package..."; Remove-Item -Path .\download\miniconda3.exe -Force; $force_install = "true"; echo "Force installation triggered!"} else { echo "No force installation necessary!"}'                                                                                    # check if url changed - if yes, remove cache and mark conda reinstall
    - 'if(-Not (Test-Path .\download\miniconda3.exe)) {echo "Downloading miniconda package..."; Invoke-WebRequest -UseBasicParsing -Uri $env:MINICONDA_URL -OutFile .\download\miniconda3.exe} else {echo "Executable found in .\download\miniconda3.exe. Reusing..."}'                                                                                                       # download the miniconda windows package
    - 'echo $env:MINICONDA_URL | Out-File -FilePath .\download\miniconda_url.txt'                                                                                                                                                                                                                                                                                                # cache version information
    - 'Get-ChildItem -Path .\download'                                                                                                                                                                                                                                                                                                                                           # show download folder content
    - '$env:PATH += ";$pwd\miniconda\condabin"'                                                                                                                                                                                                                                                                                                                                  # set path information
    - '$env:PATH += ";$pwd\miniconda\Scripts"'
    - '$env:PATH += ";$pwd\miniconda\Library\bin"'
    - '$conda_available = try {Get-Command "Get-CondaEnvironment" -errorAction Stop} catch {$null}'                                                                                                                                                                                                                                                                              # check if conda cmnd already available
    - 'if($force_install -eq "true") {$conda_available = $null}'                                                                                                                                                                                                                                                                                                                 # mark conda reinstall
    - 'if($conda_available -eq $null) {echo "conda cmnd not available! Will install in $pwd\miniconda..."; Start-Process .\download\miniconda3.exe -argumentlist "/InstallationType=JustMe /S /D=$pwd\miniconda" -wait} else {echo "Skip downloading!..."}'
    - 'echo "Environment path: $env:PATH"'
    - 'conda init'
    - 'conda config --set notify_outdated_conda false'
    - 'conda install -y mamba -c conda-forge'
    - '$progressPreference = $oldProgressPreference'
  tags:
    - windows
    - mdc
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR\.cache\pip
    ErrorActionPreference: Continue  # not working properly
    MINICONDA_URL: https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe
  cache:
    key: one-key-to-rule-them-all-windows
    paths:
      - .\download
      - .cache\pip

# Linux base template
#
# Uses a docker image where conda is already installed.
# Creates a album environment.
#
.linux_base_template:
  image: continuumio/miniconda3:latest
  tags:
    - docker
  before_script:
    - python -V  # Print out python version for debugging
    - pwd
    - which python
    - conda create -y --channel=conda-forge -n wizard python=3.9 pip git
    - conda init bash
    - source ~/.bashrc
    - apt-get update && apt-get install -y binutils
    - conda activate wizard
    - pip install pyinstaller
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
    CONDA_ENV_NAME: wizard
    CONDA_PREFIX: /opt/conda
    PREFIX: $CONDA_PREFIX/envs/$CONDA_ENV_NAME
  cache:
    key: one-key-to-rule-them-all-linux
    paths:
      - ${CONDA_PREFIX}/pkgs/*.tar.bz2
      - ${CONDA_PREFIX}/pkgs/urls.txt
      - .cache/pip

# Macos base template
#
# Installs miniconda in a fresh tmp directory and activates it.
# DO NOT ALLOW JOBS TO BE RUN IN PARALLEL!
#
.macos_base_template:
  tags:
    - macos
    - mdc
    - apple
    - catalina
  before_script:
    - echo "$(uname)"
    - sw_vers
    - tmpdir=$(mktemp -d /tmp/$CI_PROJECT_NAME-test.XXXXXX)
    - echo $tmpdir
    - echo $tmpdir > /tmp/tmpdir
    - curl https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh --output $tmpdir/miniconda.sh
    - bash $tmpdir/miniconda.sh -b -p $tmpdir/miniconda
    - export PATH=$PATH:$tmpdir/miniconda/bin/
    - echo $PATH
    - conda create -y --channel=conda-forge -n wizard python=3.7 pip git
    - source $tmpdir/miniconda/bin/activate
    - conda install -y mamba -c conda-forge
    - conda activate wizard
    - pip install pyinstaller
  after_script:
    - tmpdir=$(</tmp/tmpdir)
    - echo $tmpdir
    - rm -rf $tmpdir


stages:
  - build

linux_wizard_build:
  extends: .linux_base_template
  stage: build
  script:
    - pyinstaller $CI_PROJECT_DIR/build_wizard_linux.spec --distpath $CI_PROJECT_DIR/installer
  artifacts:
    paths:
      - $CI_PROJECT_DIR/installer/album_install_wizard


macos_wizard_build:
  extends: .macos_base_template
  stage: build
  script:
    - pyinstaller $CI_PROJECT_DIR/build_wizard_macos.spec --distpath $CI_PROJECT_DIR/installer
  artifacts:
    paths:
      - $CI_PROJECT_DIR/installer/album_install_wizard


windows_wizard_build:
  extends: .windows_base_template
  stage: build
  script:
    - $cmnd = powershell.exe -command {conda create -y --channel=conda-forge -n wizard python=3.9 pip git 2>&1 | Write-Host; conda activate wizard 2>&1 | Write-Host; pip install --no-cache-dir pyinstaller 2>&1 | Write-Host; pyinstaller $env:CI_PROJECT_DIR\build_wizard_windows.spec --distpath $env:CI_PROJECT_DIR\installer --log-level ERROR  2>&1 | Write-Host; exit(0)}
    - if (Test-Path $env:CI_PROJECT_DIR\installer\album_install_wizard.exe -PathType Leaf) {exit(0)} else {exit(1)}
  artifacts:
    paths:
      - $CI_PROJECT_DIR\installer\album_install_wizard.exe
