import os
import platform
import shutil
import subprocess
import sys
from pathlib import Path


def _install_conda_windows(conda_path):
    # install miniconda for windows
    shutil.copy(Path(os.path.realpath(__file__)).parent.joinpath("Miniconda3-latest-Windows-x86_64.exe"), conda_path)
    conda_installer = Path(conda_path).joinpath("Miniconda3-latest-Windows-x86_64.exe")
    cmd = "Start-Process %s -argumentlist \"/InstallationType=JustMe /S /D=%s\" -wait" % (
        conda_installer, conda_path)
    install_process = subprocess.run(["powershell", "-Command", cmd], capture_output=True)
    conda_exe = Path(conda_path).joinpath("condabin", "conda.bat")
    conda_exe = str(conda_exe)

    try:
        cmd = subprocess.run([conda_exe, "info"], capture_output=True)
        print("Successfully installed Miniconda.")
        Path(conda_installer).unlink()
        return conda_exe
    except Exception:
        print("An error occured when installing Conda: %s" % install_process.stderr)
        Path(conda_installer).unlink()
        return conda_exe


def _install_conda_linux(conda_path):
    # install miniconda for linux
    shutil.copy(Path(os.path.realpath(__file__)).parent.joinpath("Miniconda3-latest-Linux-x86_64.sh"), conda_path)
    conda_installer = Path(conda_path).joinpath("Miniconda3-latest-Linux-x86_64.sh")
    install_process = subprocess.run(["bash", conda_installer, "-b", "-u", "-p", conda_path, ">", "/dev/null"],
                                     capture_output=True)
    conda_exe = str(Path(conda_path).joinpath("condabin", "conda"))
    try:
        cmd = subprocess.run([conda_exe, "info"], capture_output=True)
        print("Successfully installed Miniconda.")
        Path(conda_installer).unlink()
        return conda_exe
    except Exception:
        print("An error occured when installing Conda: %s" % install_process.stderr)
        Path(conda_installer).unlink()
        return conda_exe


def _install_conda_macos(conda_path):
    # install miniconda for macos
    shutil.copy(Path(os.path.realpath(__file__)).parent.joinpath('Miniconda3-latest-MacOSX-x86_64.sh'), conda_path)
    conda_installer = Path(conda_path).joinpath('Miniconda3-latest-MacOSX-x86_64.sh')
    install_process = subprocess.run(["bash", conda_installer, "-b", "-u", "-p", conda_path, ">", "/dev/null"],
                                     capture_output=True)
    conda_exe = str(Path(conda_path).joinpath("condabin", "conda"))

    try:
        cmd = subprocess.run([conda_exe, "info"], capture_output=True)
        print("Successfully installed Miniconda.")
        Path(conda_installer).unlink()
        return conda_exe
    except Exception:
        print("An error occured when installing Conda: %s" % install_process.stderr)
        Path(conda_installer).unlink()
        return conda_exe


def install_album_full(album_base_path, conda_path, album_env_path, album_gui_url, yml_path):
    # install album and album gui and if needed miniconda
    if not Path(album_base_path).is_dir():
        Path(album_base_path).mkdir()

    print("Installing Miniconda into " + str(conda_path))
    if not Path(conda_path).is_dir():
        Path(conda_path).mkdir()

    if platform.system() == 'Windows':
        conda_exe = _install_conda_windows(conda_path)

    elif platform.system() == 'Linux':
        conda_exe = _install_conda_linux(conda_path)

    elif platform.system() == 'Darwin':
        conda_exe = _install_conda_macos(conda_path)
    else:
        print("Your OS is currently not supported")
        raise NotImplementedError

    print("-------------------------")

    print("Installing album into %s..." % album_env_path)

    a = subprocess.run([conda_exe, 'env', 'create', '-p', album_env_path, '-f', yml_path], capture_output=True)

    if a.returncode == 0:
        print("Successfully installed album.")
        print("Installing album gui...")
        g = subprocess.run([conda_exe, 'run', '-p', album_env_path, 'pip', 'install', album_gui_url])
        if g.returncode == 0:
            print("Successfully installed album gui.")
        else:
            print("An error occurred installing album gui:")
            print(g.stderr)
    else:
        print("An error occurred installing album:")
        print(a.stdout)
        print(a.stderr)
        sys.exit()


def create_activation_scripts(album_base_path, album_env_path, conda_path):
    if platform.system() == 'Linux':
        create_activation_script_linux(album_base_path, album_env_path, conda_path)
    elif platform.system() == 'Darwin':
        create_activation_script_macos(album_base_path, album_env_path, conda_path)
    elif platform.system() == 'Windows':
        create_activation_script_windows(album_base_path, album_env_path, conda_path)
    else:
        raise NotImplementedError("Your OS is currently not supported. Aborting...")


def create_deactivation_scripts(album_base_path, album_env_path, conda_path):
    if platform.system() == 'Linux':
        create_deactivation_script_linux(album_base_path, album_env_path, conda_path)
    elif platform.system() == 'Darwin':
        create_deactivation_script_macos(album_base_path, album_env_path, conda_path)
    elif platform.system() == 'Windows':
        create_deactivation_script_windows(album_base_path, album_env_path, conda_path)
    else:
        raise NotImplementedError("Your OS is currently not supported. Aborting...")


def create_activation_script_linux(album_base_path, album_env_path, conda_path):
    file_string = """
    #!/bin/sh\n
    source %s/bin/activate %s\n
    """ % (conda_path, album_env_path)
    with open(Path(album_base_path).joinpath('activate_album.sh'), 'w') as file:
        file.write(file_string)


def create_activation_script_macos(album_base_path, album_env_path, conda_path):
    file_string = """
    #!/bin/sh\n
    source %s/bin/activate %s\n
    """ % (conda_path, album_env_path)
    with open(Path(album_base_path).joinpath('activate_album.sh'), 'w') as file:
        file.write(file_string)


def create_activation_script_windows(album_base_path, album_env_path, conda_path):
    file_string = """@echo off\nset MINICONDA_PATH=""" + str(
        conda_path) + """\nset CONDA_EXECUTABLE=%MINICONDA_PATH%\\condabin\\conda\nset ALBUM_ENV=""" + str(
        album_env_path) + """ \nCALL %CONDA_EXECUTABLE% activate %ALBUM_ENV%\ncmd /k"""
    with open(Path(album_base_path).joinpath('activate_album.bat'), 'w') as file:
        file.write(file_string)


def create_deactivation_script_linux(album_base_path, album_env_path, conda_path):
    file_string = """
    #!/bin/sh\n
    source %s/bin/deactivate\n
    """ % conda_path
    with open(Path(album_base_path).joinpath('deactivate_album.sh'), 'w') as file:
        file.write(file_string)


def create_deactivation_script_macos(album_base_path, album_env_path, conda_path):
    file_string = """
    #!/bin/sh\n
    source %s/bin/deactivate\n
    """ % conda_path
    with open(Path(album_base_path).joinpath('deactivate_album.sh'), 'w') as file:
        file.write(file_string)


def create_deactivation_script_windows(album_base_path, album_env_path, conda_path):
    file_string = """@echo off\nset MINICONDA_PATH=""" + str(
        conda_path) + """\nset CONDA_EXECUTABLE=%MINICONDA_PATH%\\condabin\\conda\nCALL %CONDA_EXECUTABLE% deactivate\ncmd /k"""
    with open(Path(album_base_path).joinpath('deactivate_album.bat'), 'w') as file:
        file.write(file_string)


def main():
    album_base_path = Path.home().joinpath('.album')
    conda_path = Path(album_base_path).joinpath("Miniconda")
    album_env_path = Path(album_base_path).joinpath('envs', 'album')
    album_env_path = str(album_env_path)
    album_gui_url = "https://gitlab.com/album-app/plugins/album-gui/-/archive/main/album-gui-main.zip"
    yml_path = Path(os.path.realpath(__file__)).parent.joinpath('album.yml')
    yml_path = str(yml_path)

    install_album_full(album_base_path, conda_path, album_env_path, album_gui_url, yml_path)
    create_activation_scripts(album_base_path, album_env_path, conda_path)
    create_deactivation_scripts(album_base_path, album_env_path, conda_path)

    input("Installation completed. Press return to exit...")

if __name__ == "__main__":
    main()
